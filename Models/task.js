const mongoose = require('mongoose')

const taskSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name is required"]
	},
	status: {
		type: String,
		default: "Pending"
	}
})

module.exports = mongoose.model('Task', taskSchema)