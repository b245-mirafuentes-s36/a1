const express = require('express')
const router = express.Router();

const taskController = require("../Controllers/taskController.js")

/*
Routes
*/

//Route for getAll

router.get("/get", taskController.getAll)

// Route for createTask

router.post("/addTask", taskController.createTask)

// Route for deleteTask
router.delete("/deleteTask/:id", taskController.deleteTask)

// Activity
router.get("/:id", taskController.getOne)

router.put("/:id/complete", taskController.changeStats)


module.exports = router