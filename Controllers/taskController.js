const Task = require("../Models/task.js")

/*Controllers and functions*/

//Controller/function to get all the task on our database
module.exports.getAll = (request, response) =>{
	Task.find({})
	// to capture the result of the find method
	.then(result =>{
		return response.send(result)
	})
	// .catch method captures the error when the find method is executed
	.catch(error => {
		return response.send(error)
	})
}

// Add Task on out database
module.exports.createTask = (request, response) => {
	const input = request.body
	Task.findOne({name: input.name})
	.then(result => {
		if(result !== null){
			return response.send("The task is already existing")
		}else{
			let newTask = new Task({
				name: input.name
			});
			newTask.save().then(save => {
				return response.send("the task successfully added")
			}).catch(error => {
				return response.send(error)
			})
		}
	})
	.catch(error => {
		return response.send(error)
	})
}

// Controller that will delete the document that contains the given object

module.exports.deleteTask = (request, response) => {
	let idToBeDeleted = request.params.id

	//findbyIdAndRemove - to find the document that contains the id and then delete the document

	Task.findByIdAndRemove(idToBeDeleted)
	.then(result => {
		return response.send(result)
	})
	.catch(error => {
		return response.send(error)
	})
}

// Activity
module.exports.getOne = (request, response) => {
	let getId = request.params.id
	Task.findOne({_id: getId})
	.then(result => {
		return response.send(result)
	})
	.catch(error => {
		return response.send(error)
	})
}

module.exports.changeStats = (request, response) => {
	let getId = request.params.id
	Task.findByIdAndUpdate(getId, {status: "complete"}, {new: true})
	.then(result => {
		return response.send(result)
	})
	.catch(error => {
		return response.send(error)
	})
}