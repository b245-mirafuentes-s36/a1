const express = require('express')
const mongoose = require('mongoose')

const taskRoute = require("./Routes/taskRoute.js")

const app = express()
const port = 3001


	// MongoDB Connection
	mongoose.connect("mongodb+srv://admin:admin@batch245-mirafuentes.m5uejs3.mongodb.net/s35-discussion?retryWrites=true&w=majority",{
		useNewUrlParser: true,
		useUnifiedTopology: true
	})

	//Check Connection

	let db = mongoose.connection

	// error handling
	db.on('error', console.error.bind(console, "Connection Error"))

	//Confirmation of the connection
	db.once('open', () => console.log('We are now connected to the cloud'))



//middlewares
app.use(express.json())
app.use(express.urlencoded({extended: true}))


//routing
app.use("/task", taskRoute)








app.listen(port, console.log(`Server is running at port ${port}`))

/*
	Separation of Concerns
		1. Model should be connected to the controller.
		2. Controller should be connected to the ROutes
		3. Route should be connected to the server/application
*/